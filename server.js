'use strict';
// Funny part hides inside /component

const express = require('express');
const helmet = require('helmet');
const path = require('path');
const component = require('./component');
const expressErrorHandler = require('./lib/express/errorHandlerTerminator');
const winston = require('winston');
const expressWinston = require('express-winston');

process.env.DATA_DIRECTORY = path.join(__dirname, 'data');

// === Configure logger
let transports;

switch (process.env.NODE_ENV) {
    case 'development':
        transports = [
            new winston.transports.Console({
                level: 'debug',
                colorize: true,
            })
        ];
        break;
    case 'testing':
        transports = [
            new winston.transports.Console({
                level: 'info',
                colorize: true,
                silent: true
            })
        ];
        break;
    default:
        transports = [
            new winston.transports.Console({
                level: 'info',
                colorize: true,
            })
        ];
}

winston.configure({
    transports
});

// ==== Init Express
const app = express();
app.use(helmet());
app.use(expressWinston.logger({
    transports,
    msg: "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.path}} {{JSON.stringify(req.params)}} {{JSON.stringify(req.query)}}",
    meta: false,
    colorize: true,
}));

//API Routes
app.use('/', component.routes);

app.use(expressErrorHandler());
app.use(expressWinston.errorLogger({
    transports,
}));

const server = app.listen(process.env.PORT || 8888, () => {
    winston.info('Tile server is listening at ' + server.address().port);
    winston.info('Environment ' + process.env.NODE_ENV);

    // This server inits faster than mocha test suite.
    let remainingEvents = 5;
    while (remainingEvents) {
        setTimeout(() => {
            server.emit("appStarted");
        }, 500 * remainingEvents);
        remainingEvents -= 1;
    }
});
module.exports = server;
