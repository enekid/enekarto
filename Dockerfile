FROM afrith/node-xenial

RUN apt-get update && apt-get install -y gdal-bin libmapnik-dev

WORKDIR /srv/service

CMD ["npm", "run", "dev"]