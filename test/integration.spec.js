const chai = require('chai');
const request = require('supertest');
const expect = chai.expect;

describe('API endpoint', function() {
    it('should return tile', function(done) {
        request(process.env.API_URL)
            .get('/admin0/0/0/0')
            .end(function(err, res) {
                expect(res.statusCode).to.equal(200);
                done();
            });
    });
    it('should return 422 if style not valid', function(done) {
        request(process.env.API_URL)
            .get('/admin3/0/0/0')
            .end(function(err, res) {
                expect(res.statusCode).to.equal(422);
                expect(res.body.code).to.equal('invalid-style');
                expect(res.body.data).to.equal('admin3');
                done();
            });
    });
    it('should return 422 if zoom level less than 0', function(done) {
        request(process.env.API_URL)
            .get('/admin0/-1/0/0')
            .end(function(err, res) {
                expect(res.statusCode).to.equal(422);
                expect(res.body.code).to.equal('invalid-zoom');
                done();
            });
    });
    it('should return 404 if endpoint doesn\'t exit', function(done) {
        request(process.env.API_URL)
            .get('/asdf')
            .end(function(err, res) {
                expect(res.statusCode).to.equal(404);
                done();
            });
    });
});