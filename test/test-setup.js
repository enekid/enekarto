process.env.API_URL = 'http://localhost:8888';

const app = require('../server');

before((next) => {
    app.once("appStarted", function(){
        next()
    });
});

after((next) => {
    next();
});

beforeEach(function () {
});

afterEach(function () {
});