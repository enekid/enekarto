# CARTO Node.js code test - Eneko

## Introduction

To run the server you have 2 options: Docker Compose and Npm. The server can be accessed at [http://localhost:8888](http://localhost:8888)

If you open `index.html` in your browser you will see a world map with 2 layers.

### Docker Compose

```bash
docker-compose up --build
```

The required containers will download automatically and the server will start afterwards.

### Npm

Your system must meet the [requirements](https://github.com/mapnik/node-mapnik#depends) to run Mapnick.

```bash
npm run dev
```

All the node modules will be installed and the server will start afterwards.

For production you can use:

```bash
npm start
```

## Tests

Run the tests with:

```bash
npm test
```

or

uncomment the `command` line at `docker-compose.yml` and run

```bash
docker-compose up
```

## Documentation

A Postman collection with API calls can be found at `doc/eneKarto.postman_collection.json`

An [Open Api](https://www.openapis.org/) `yaml` can be found at `doc/enekarto-swagger.yaml`. A basic compilation of this specification: `doc/enekarto-swagger.html`
