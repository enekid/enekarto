'use strict';

const express = require('express');
const Router = express.Router();
const createError = require('http-errors');
const Controller = require('./controller');

const validateParams = ({style, z, x, y}, req) => {
    if (! style) {
        throw(new createError.UnprocessableEntity({code: 'invalid-style', data: req.params.style}));
    }
    if (! Number.isInteger(z)) {
        throw(new createError.UnprocessableEntity({code: 'invalid-z', data: req.params.z}));
    }
    if (z < 0) {
        throw(new createError.UnprocessableEntity({code: 'invalid-zoom', data: req.params.z}));
    }
    if (! Number.isInteger(x)) {
        throw(new createError.UnprocessableEntity({code: 'invalid-x', data: req.params.x}));
    }
    if (! Number.isInteger(y)) {
        throw(new createError.UnprocessableEntity({code: 'invalid-y', data: req.params.yRegexed}));
    }
};

Router.get('/:style/:z/:x/:y', (req, res, next) => {
        const style = Controller.getStyle(req.params.style);

        const z = Number(req.params.z);
        const x = Number(req.params.x);
        req.params.yRegexed = req.params.y.replace(/(\.png.*)/gi, ''); //remove trailing .png
        const y = Number(req.params.yRegexed);

        validateParams({style, z, x, y}, req);

        Controller.getTile(
            style,
            z,
            x,
            y
        ).then((response) => {
            res.setHeader("Content-Type", "image/png");
            res.status(200).send(response);
            next();
        }).catch((err) => next(err));
});

module.exports = Router;
