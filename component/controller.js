'use strict';

const fs = require('fs');
const mapnik = require('mapnik');
const path = require('path');
const winston = require('winston')

let Controller = {};

mapnik.register_datasource(path.join(mapnik.settings.paths.input_plugins, 'shape.input'));

const TILE_SIZE = 256;

const styles = {
    admin0: "style-admin0.xml",
    admin1: "style-admin1.xml",
};

Controller.getStyle = (style) => {
   return styles[style];
};

Controller.getTile = (style, z, x, y) => {
    winston.debug("Get tile: ", style,z,x,y);
    const promise = new Promise((resolve, reject) => {
        try {
            const map = new mapnik.Map(TILE_SIZE, TILE_SIZE);
            const dataPath = path.join(process.env.DATA_DIRECTORY, style);
            map.load(dataPath, function(err, map) {
                const vt = new mapnik.VectorTile(z,x,y);
                const im = new mapnik.Image(TILE_SIZE, TILE_SIZE);

                map.render(vt, {}, function(err, vtile) {
                    if (err) { return reject(err); }

                    vtile.render(map, im, function(err, image) {
                        if (err) { return reject(err); }

                        image.encode('png', function(err, buffer) {
                            if (err) { return reject(err); }

                            resolve(buffer);
                        });
                    })
                })
            });
        } catch (err) {
            return reject(err);
        }
    });

    return promise;
};

module.exports = Controller;
