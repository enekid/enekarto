const fs = require('fs');
const mapnik = require('mapnik');
const path = require('path');

mapnik.register_datasource(path.join(mapnik.settings.paths.input_plugins, 'shape.input'));

const TILE_SIZE = 256;

const map = new mapnik.Map(TILE_SIZE, TILE_SIZE);
map.load('./style-admin0.xml', function(err, map) {
    const vt = new mapnik.VectorTile(2,0,0);
    const im = new mapnik.Image(TILE_SIZE, TILE_SIZE);

    map.render(vt, {}, function(err, vtile) {
        if (err) console.log(err)

        vtile.render(map, im, function(err, image) {
            if (err) console.log(err)

            image.encode('png', function(err, buffer) {
                if (err) console.log(err)
                fs.writeFile('./map.png', buffer);
            });
        })
    })
});