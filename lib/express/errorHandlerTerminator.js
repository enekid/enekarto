'use strict';

const winston = require('winston');

module.exports = () => {
    return (err, req, res, next) => {
        if (err) {
            winston.error(err.message);
            if (err.message) {
                return res.status(err.statusCode).send(err.message);
            }

            return res.status(500).send('Server Error');
        }
    };
};